# TD02_reverse_6LoWPAN

Vous avez reçu un objet et vous voulez comprendre ce qu'il fait.


***Documentez les étapes que vous avez réalisées, à remettre par mail à la fin du TP***


- Analysez la sortie UART
- Dumper le firmware à l'aide de la sonde de programmation
- Sur quel serveur les messages sont ils publiés ?
- Dans quel topic ?
- Changer le message publié
- Il a 2 autres fonctions sur cet objet, quelle sont-elles ? Comment les utiliser ?


Vous pouvez vous aider d'outils tels que screen, strings, ghidra, mosquitto-clients.
(le port utilisé est le 1883)
